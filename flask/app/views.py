from flask import render_template
from app import app
from flask import request


from sklearn.linear_model import SGDClassifier
from sklearn.externals import joblib
from sklearn.feature_extraction.text import TfidfVectorizer


clf_sgd = joblib.load('app/clf_sgd.pkl') 
vectorizer = joblib.load('app/vec.pkl')

@app.route('/')
@app.route('/index')
def index():

    return render_template("index.html",
        version = '0.1')

@app.route('/show', methods=['POST'])
def show_prediction():
    #if not session.get('logged_in'):
    #    abort(401)
    #g.db.execute('insert into entries (title, text) values (?, ?)',
    #             [request.form['title'], request.form['text']])
    #g.db.commit()
    #flash('New entry was successfully posted')
    prediction = clf_sgd.predict(vectorizer.transform([request.form['text']]))

    return render_template("show_prediction.html",
        version = '0.1',
        description = request.form['text'],
        prediction = prediction[0])